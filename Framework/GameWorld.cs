﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace mxGame.Framework
{
	public abstract class GameWorld
	{
		public class EntityComponentRegister
		{
			public GameEntity Entity => _entity;
			private GameEntity _entity;

			public List<Component> Components => _components;
			private List<Component> _components;

			public EntityComponentRegister(GameEntity entity)
			{
				_entity = entity;
				_components = new List<Component>();
			}
		}

		public interface IComponentSystem<T>
		{
			/// <summary>
			/// handle all the component systems that are implemented in the game world
			/// </summary>
			public void HandleComponentSystems();

			/// <summary>
			/// get the components of the specified entities that implement the type that this component 
			/// system handles
			/// </summary>
			/// <param name="entityIDs"></param>
			public IReadOnlyList<T> GetSystemComponents(IReadOnlyList<uint> entityIDs);
		}

		/// <summary>
		/// A basic GameWorld type that implements Update and Draw component systems
		/// </summary>
		public class Default : GameWorld, IComponentSystem<Component.IUpdateable>, IComponentSystem<Component.IDrawable>, IComponentSystem<Component.IRenderer>
		{
			private Dictionary<uint, List<Component.IUpdateable>> _updateableComponents;
			private Dictionary<uint, List<Component.IDrawable>> _drawableComponents;
			private Dictionary<uint, List<Component.IRenderer>> _rendererComponents;

			private GameTime _currentTime;
			private SpriteBatch _renderBatch;
			private GraphicsDevice _graphicsDevice;

			/// <summary>
			/// only entities who's bounding rectangle overlap the UpdateArea will be updated
			/// </summary>
			public RectangleF UpdateArea { get; set; }

			/// <summary>
			/// only entities who's bounding rectangle overlap the DrawArea will be drawn
			/// </summary>
			public RectangleF DrawArea { get; set; }

			/// <summary>
			/// only entities who's bounding rect overlat the RenderArea will be rendered
			/// </summary>
			public RectangleF RenderArea { get; set; }

			/// <inheritdoc/>
			public Default() : base()
			{
				_updateableComponents = new Dictionary<uint, List<Component.IUpdateable>>();
				_drawableComponents = new Dictionary<uint, List<Component.IDrawable>>();
				_rendererComponents = new Dictionary<uint, List<Component.IRenderer>>();
			}

			/// <summary>
			/// Handle the Update and Draw component systems with the provided data from 
			/// <see cref="SetComponentSystemData(GameTime, SpriteBatch)"/>
			/// </summary>
			public void HandleComponentSystems() 
			{
				// create lists to hold the UIDs of entities that are to be updated or drawn
				List<uint> updateIds = new List<uint>();
				List<uint> drawIds = new List<uint>();
				List<uint> renderIds = new List<uint>();

				// TODO: optomize with spatial partitioning
				// iterate through each entity in the registry
				foreach(var pair in _entityRegistry)
				{
					// check to see if the entity overlaps with the effective area for updating or drawing and 
					// add it's ID to the corresponding list
					uint entityID = pair.Key;
					RectangleF bounds = GetEntityBoundingRectangle(entityID);
					if (UpdateArea.Intersects(bounds)) updateIds.Add(entityID);
					if (DrawArea.Intersects(bounds)) drawIds.Add(entityID);
					if (RenderArea.Intersects(bounds)) renderIds.Add(entityID);
				}

				var updateables = (this as IComponentSystem<Component.IUpdateable>).GetSystemComponents(updateIds);
				var drawables = (this as IComponentSystem<Component.IDrawable>).GetSystemComponents(drawIds);
				var renderers = (this as IComponentSystem<Component.IRenderer>).GetSystemComponents(renderIds);

				// TODO get refs for gametime and spritebatch
				for (int i = updateables.Count - 1; i >= 0; i--)
					updateables[i].Update(_currentTime);
				for (int i = drawables.Count - 1; i >= 0; i--)
					drawables[i].Draw(_currentTime, _renderBatch);
				for (int i = renderers.Count - 1; i >= 0; i--)
					renderers[i].Render(_currentTime, _graphicsDevice);

			}

			IReadOnlyList<Component.IUpdateable> IComponentSystem<Component.IUpdateable>.GetSystemComponents(IReadOnlyList<uint> entityIDs)
			{
				List<Component.IUpdateable> r = new List<Component.IUpdateable>();

				for (int i = entityIDs.Count - 1; i >= 0; i--)
				{
					if (!_updateableComponents.ContainsKey(entityIDs[i]))
						continue;

					List<Component.IUpdateable> entityDrawables = _updateableComponents[entityIDs[i]];
					r.AddRange(entityDrawables);
				}

				return r;
			}
			IReadOnlyList<Component.IDrawable> IComponentSystem<Component.IDrawable>.GetSystemComponents(IReadOnlyList<uint> entityIDs)
			{
				List<Component.IDrawable> r = new List<Component.IDrawable>();
				
				for(int i = entityIDs.Count - 1; i >= 0; i--)
				{
					if (!_drawableComponents.ContainsKey(entityIDs[i]))
						continue;

					List<Component.IDrawable> entityDrawables = _drawableComponents[entityIDs[i]];
					r.AddRange(entityDrawables);
				}

				return r;
			}
			IReadOnlyList<Component.IRenderer> IComponentSystem<Component.IRenderer>.GetSystemComponents(IReadOnlyList<uint> entityIDs)
			{
				List<Component.IRenderer> r = new List<Component.IRenderer>();

				for (int i = entityIDs.Count - 1; i >= 0; i--)
				{
					if (!_rendererComponents.ContainsKey(entityIDs[i]))
						continue;

					List<Component.IRenderer> entityDrawables = _rendererComponents[entityIDs[i]];
					r.AddRange(entityDrawables);
				}

				return r;
			}

			protected override void OnEntityRemoved(uint entityID)
			{
				if (_updateableComponents.ContainsKey(entityID))
					_updateableComponents.Remove(entityID);

				if (_drawableComponents.ContainsKey(entityID))
					_drawableComponents.Remove(entityID);

				if (_rendererComponents.ContainsKey(entityID))
					_rendererComponents.Remove(entityID);
			}
			protected override void OnComponentAdded(Component component)
			{
				uint entityID = component.Entity.Uid;

				if (component is Component.IUpdateable updateable)
				{
					if (!_updateableComponents.ContainsKey(entityID))
						_updateableComponents.Add(entityID, new List<Component.IUpdateable>());
					_updateableComponents[entityID].Add(updateable);
				}

				if (component is Component.IDrawable drawable)
				{
					if (!_drawableComponents.ContainsKey(entityID))
						_drawableComponents.Add(entityID, new List<Component.IDrawable>());
					_drawableComponents[entityID].Add(drawable);
				}

				if (component is Component.IRenderer renderer)
				{
					if (!_rendererComponents.ContainsKey(entityID))
						_rendererComponents.Add(entityID, new List<Component.IRenderer>());
					_rendererComponents[entityID].Add(renderer);
				}
			}
			protected override void OnComponentRemoved(Component component)
			{
				uint entityID = component.Entity.Uid;

				if (component is Component.IUpdateable updateable)
				{
					List<Component.IUpdateable> updateables = _updateableComponents[entityID];
					updateables.Remove(updateable);
					if (updateables.Count <= 0)
						_entityRectangleEffectors.Remove(entityID);
				}

				if (component is Component.IDrawable drawable)
				{
					List<Component.IDrawable> drawables = _drawableComponents[entityID];
					drawables.Remove(drawable);
					if (drawables.Count <= 0)
						_drawableComponents.Remove(entityID);
				}

				if (component is Component.IRenderer renderer)
				{
					List<Component.IRenderer> renderers = _rendererComponents[entityID];
					renderers.Remove(renderer);
					if (renderers.Count <= 0)
						_rendererComponents.Remove(entityID);
				}
			}

			/// <summary>
			/// gives the game world data about the current gametime and what spritebatch should 
			/// be used for rendering. Must be called immediately before each call to
			/// <see cref="HandleComponentSystems()"/>
			/// </summary>
			/// <param name="gametime">the current gametime</param>
			/// <param name="batch">the spritebatch used for rendering</param>
			public void SetComponentSystemData(GameTime gametime, SpriteBatch batch, GraphicsDevice graphicsDevice)
			{
				_currentTime = gametime;
				_renderBatch = batch;
				_graphicsDevice = graphicsDevice;
			}
		}

		/// <summary>
		/// the root scene node of the scene graph
		/// </summary>
		public WorldNode RootNode => _rootNode;
		private WorldNode _rootNode;

		protected Dictionary<uint, EntityComponentRegister> _entityRegistry;
		protected Dictionary<uint, List<Component.IBoundingRectangleEffector>> _entityRectangleEffectors;

		/// <summary>
		/// creates a new instance of a GameWorld
		/// </summary>
		public GameWorld()
		{
			_entityRegistry = new Dictionary<uint, EntityComponentRegister>();
			_entityRectangleEffectors = new Dictionary<uint, List<Component.IBoundingRectangleEffector>>();
			_rootNode = new WorldNode(this);
		}

		/// <summary>
		/// caculates the bounding rect of the entity with the specified UID
		/// </summary>
		/// <param name="entityID">the ID of the entity to get the bounding box of</param>
		public RectangleF GetEntityBoundingRectangle(uint entityID) 
		{
			// if there are no rectangleEffectors return a bounding box of size zero at the entity position
			if (!_entityRectangleEffectors.ContainsKey(entityID))
				return new RectangleF(_entityRegistry[entityID].Entity.WorldPosition, Vector2.Zero);

			// get all the components that effect the entity bounding box
			List<Component.IBoundingRectangleEffector> rectBoundEfs = _entityRectangleEffectors[entityID];

			// if there are no rectangleEffectors return a bounding box of size zero at the entity position
			if(rectBoundEfs.Count <= 0)
				return new RectangleF(_entityRegistry[entityID].Entity.WorldPosition, Vector2.Zero);

			// get the min/max values of all the bouning boxes in each component on the entity
			Vector2 min = rectBoundEfs[0].BoundingRectangle.TopLeft;
			Vector2 max = rectBoundEfs[0].BoundingRectangle.BottomRight;
			for(int i = rectBoundEfs.Count - 1; i > 0; i--)
			{
				RectangleF boundRect = rectBoundEfs[i].BoundingRectangle;
				min.X = MathF.Min(min.X, boundRect.X);
				min.Y = MathF.Min(min.Y, boundRect.Y);
				max.X = MathF.Max(max.X, boundRect.Right);
				max.Y = MathF.Max(max.Y, boundRect.Bottom);
			}

			// create a new rectangle from the min and max values and return it
			return new RectangleF(min, max - min);
		}

		/// <summary>
		/// creates an entity in the world and returns it
		/// </summary>
		public GameEntity CreateEntity(string name)
		{
			GameEntity entity = new GameEntity(this, name);
			return entity;
		}

		/// <summary>
		/// Adds the specified entity to the world
		/// </summary>
		/// <param name="entity">the entity to add to the world</param>
		public void AddEntity(GameEntity entity)
		{
			_entityRegistry.Add(entity.Uid, new EntityComponentRegister(entity));
			_rootNode.Children.Add(entity.Node);
		}

		/// <summary>
		/// removes the specified entity from the world
		/// </summary>
		/// <param name="entityID"></param>
		public void RemoveEntity(uint entityID)
		{
			// remove entity from scene graph
			EntityComponentRegister register = _entityRegistry[entityID];
			WorldNode node = register.Entity.Node;
			node.RemoveNode();

			// remove entity from world entity register
			_entityRegistry.Remove(entityID);
			_entityRectangleEffectors.Remove(entityID);

			OnEntityRemoved(entityID);
		}

		/// <summary>
		/// adds a new component of the specified type to the entity with the specified id and
		/// returns it
		/// </summary>
		/// <typeparam name="T">the component type to add</typeparam>
		/// <param name="entityID">the id of the entity to add the component to</param>
		/// <returns></returns>
		public T AddEntityComponent<T>(uint entityID) where T : Component, new()
		{
			T component = new T();

			EntityComponentRegister register = _entityRegistry[entityID];
			register.Components.Add(component);
			component.AttachToEntity(register.Entity);

			if(component is Component.IBoundingRectangleEffector rectComp)
			{
				if(!_entityRectangleEffectors.ContainsKey(entityID))
					_entityRectangleEffectors.Add(entityID, new List<Component.IBoundingRectangleEffector>());
				_entityRectangleEffectors[entityID].Add(rectComp);
			}

			OnComponentAdded(component);
			component.Initialize();

			return component;
		}

		/// <summary>
		/// removes the component from it's entity and the world
		/// </summary>
		/// <param name="component">the component to remove</param>
		public void RemoveEntityComponent(Component component)
		{
			uint entityID = component.Entity.Uid;
			EntityComponentRegister register = _entityRegistry[entityID];
			register.Components.Remove(component);

			if(component is Component.IBoundingRectangleEffector rectComp)
			{
				List<Component.IBoundingRectangleEffector> rects = _entityRectangleEffectors[entityID];
				rects.Remove(rectComp);
				if (rects.Count <= 0)
					_entityRectangleEffectors.Remove(entityID);
			}

			OnComponentRemoved(component);
		}

		/// <summary>
		/// implement to handle removing components from component systems when an entity is 
		/// removed from the world
		/// </summary>
		/// <param name="entityID">the UID if the entity that was removed</param>
		protected abstract void OnEntityRemoved(uint entityID);

		/// <summary>
		/// implement to add components to the proper component systems when a component is
		/// attached to an entity and added to the world
		/// </summary>
		/// <param name="component">the component that was added</param>
		protected abstract void OnComponentAdded(Component component);

		/// <summary>
		/// implement to remove components from the proper component systems when a component
		/// is removed from an entity and the world
		/// </summary>
		/// <param name="component">the component that was removed</param>
		protected abstract void OnComponentRemoved(Component component);
	}
}
