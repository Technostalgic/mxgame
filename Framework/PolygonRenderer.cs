﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace mxGame.Framework
{
	class PolygonRenderer : Renderer
	{
		public IList<Vector2> Vertices { get; set; } = new List<Vector2>();
		public Color Color { get; set; } = Color.Red;
		public float LineThickness { get; set; } = 2;
		public float LayerDepth { get; set; } = 0;

		public override IRendererEffect Effect { 
			get => PrimitiveRenderer.Effect as IRendererEffect;
			set => throw new Exception("Effect for primitive renderers cannot be set");
		}

		public PolygonRenderer()
		{
			if (PrimitiveRenderer == null)
				throw new Exception("PrimitiveRenderer has not been created");
		}

		private Vector2[] _tempVerts;

		/// <inheritdoc/>
		override public void Render(GameTime gameTime, GraphicsDevice graphicsDevice)
		{
			if (_tempVerts == null || _tempVerts.Length != Vertices.Count)
				_tempVerts = new Vector2[Vertices.Count];

			for(int i = Vertices.Count - 1; i >= 0; i--)
				_tempVerts[i] = Node.WorldMatrix.Transform(Vertices[i]);

			PrimitiveRenderer.DrawPolygonFill(_tempVerts, Color);
		}
	}
}
