﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoShapes;

namespace mxGame.Framework
{
	public abstract class Renderer : Component, Component.IRenderer
	{
		public interface IRendererEffect : IEffectMatrices
		{
			public EffectParameterCollection Parameters { get; }
			public EffectTechniqueCollection Techniques { get; }
			public EffectTechnique CurrentTechnique { get; set; }
		}

		public class BasicRendererEffect : BasicEffect, IRendererEffect
		{
			public BasicRendererEffect(GraphicsDevice graphicsDevice) : base(graphicsDevice) { }
		}

		/// <summary>
		/// The default effect that a renderer will use to render an object
		/// </summary>
		public static IRendererEffect DefaultEffect { get; set; }

		/// <summary>
		/// The shaperenderer object used to render primitives
		/// </summary>
		public static ShapeRenderer PrimitiveRenderer { get; set; }

		/// <summary>
		/// The effect used to render this renderer, leave null to use the default renderer effect
		/// </summary>
		public virtual IRendererEffect Effect { 
			get => _customEffect ?? DefaultEffect ?? throw new Exception("Renderer.DefaultEffect has not been set");
			set => _customEffect = value;
		}
		protected IRendererEffect _customEffect;

		public abstract void Render(GameTime gameTime, GraphicsDevice graphicsDevice);
	}
}
