﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace mxGame.Framework
{
	public static class MXExtensions
	{
		/// <summary>
		/// sets the center of the camera to the specified position
		/// </summary>
		/// <param name="position">the position to center the camera at</param>
		public static void CenterAt(this OrthographicCamera cam, Vector2 position)
		{
			// position the camera at the specified position and offset it by half the 
			// camera viewport size
			cam.Position = position - (Vector2)cam.BoundingRectangle.Size / 2f;
		}

		/// <summary>
		/// gets all the sprite rects that fit in the texture of the specified size
		/// </summary>
		/// <param name="texture">the texture to use</param>
		/// <param name="width">the width of the sprite</param>
		/// <param name="height">the height of the sprite</param>
		public static Rectangle[] GetSpriteRects(this Texture2D texture, int width, int height)
		{
			int sizeX = texture.Width / width;
			int sizeY = texture.Height / height;

			Rectangle[] rects = new Rectangle[sizeX * sizeY];
			for(int y = 0; y < sizeY; y++)
			{
				for(int x = 0; x < sizeX; x++)
				{
					rects[y * sizeX + x] = new Rectangle(x * width, y * height, width, height);
				}
			}

			return rects;
		}

		public static T[] CreateDefaultArray<T>(this T def, int size) where T : struct
		{
			T[] arr = new T[size];
			
			for(int i = size - 1; i >= 0; i--)
			{
				arr[i] = def;
			}

			return arr;
		}
	}
}
