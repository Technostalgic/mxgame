﻿using Microsoft.Xna.Framework;
using MonoGame.Framework;
using MonoGame.Extended;
using MonoGame.Extended.SceneGraphs;

namespace mxGame.Framework
{
	public class WorldNode : SceneNode, Component.IGameEntityable
	{
		/// <summary>
		/// A reference to the node that is one level above this node in the world graph hierarchy
		/// </summary>
		new public WorldNode Parent
		{
			get => base.Parent as WorldNode;
			set => base.Parent = value;
		}

		/// <summary>
		/// get or set the position of the node in the world
		/// </summary>
		new public Vector2 WorldPosition {
			get => base.WorldPosition;
			set {
				Matrix2 invWorldMat =  Matrix2.Invert(Parent.WorldMatrix);
				Position = invWorldMat.Transform(value);
			}
		}

		/// <summary>
		/// get or set the rotation of the node in the world
		/// </summary>
		new public float WorldRotation
		{
			get => base.WorldRotation;
			set {
				Matrix2 invWorldMat = Matrix2.Invert(Parent.WorldMatrix);
				Rotation = -invWorldMat.Rotation + value;
			}
		}

		/// <summary>
		/// the root node of the world that this node exists in
		/// </summary>
		public WorldNode Root => _world.RootNode;

		/// <summary>
		/// the world that the node exists within
		/// </summary>
		public GameWorld World => _world;
		private GameWorld _world;

		/// <summary>
		/// the game entity that this node is attached to
		/// </summary>
		public GameEntity Entity {
			get => _entity;
			set
			{
				if (_entity == value)
					return;

				GameEntity oent = _entity;
				_entity = value;

				if (value != null)
					value.Node = this;
				if (oent != null)
					oent.Node = null;
			}
		}
		private GameEntity _entity;

		/// <summary>
		/// create a new WorldNode instance
		/// </summary>
		protected WorldNode() : base() { }

		/// <summary>
		/// create a new WorldNode instance
		/// </summary>
		public WorldNode(GameWorld world)
		{
			_world = world;
		}

		/// <summary>
		/// creates a node as a child of this node
		/// </summary>
		public WorldNode CreateChild()
		{
			WorldNode r = new WorldNode();

			r._world = _world;
			Children.Add(r);
			r.Parent = this;

			return r;
		}

		/// <summary>
		/// creates an entity attached to this node
		/// </summary>
		public GameEntity CreateEntity(string name = "GameEntity")
		{
			if (_entity != null)
				throw new System.Exception("World node already has entity attached");

			GameEntity r = new GameEntity(_world, name);

			r.Node = this;

			return r;
		}

		/// <summary>
		/// creates a child node and attaches an entity to it
		/// </summary>
		public GameEntity CreateChildEntity(string name = "GameEntity")
		{
			return CreateChild().CreateEntity(name);
		}

		/// <summary>
		/// removes the node from the scene graph, essentially destroying it, this should 
		/// generally not be called by the user, see GameEntity.RemoveEntity() if you are
		/// trying to remove an entity from the world
		/// </summary>
		public void RemoveNode()
		{
			Parent.Children.Remove(this);
			_world = null;
		}
	}
}
