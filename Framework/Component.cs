﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace mxGame.Framework
{
	public abstract class Component : Component.IGameEntityable
	{
		public interface IGameEntityable
		{
			public GameEntity Entity { get; }
		}

		public interface IBoundingRectangleEffector : IGameEntityable
		{
			public RectangleF BoundingRectangle { get; }
		}

		public interface IUpdateable : IGameEntityable
		{
			public void Update(GameTime gameTime);
		}

		public interface IDrawable : IGameEntityable
		{
			public void Draw(GameTime gameTime, SpriteBatch batch);
			public float LayerDepth { get; set; }
		}

		public interface IRenderer : IGameEntityable
		{
			public void Render(GameTime gameTime, GraphicsDevice graphicsDevice);
		}

		public WorldNode Node => _entity.Node;

		public GameEntity Entity => _entity;
		private GameEntity _entity;

		public Component() { }

		public virtual void Initialize() { }

		internal void AttachToEntity(GameEntity entity)
		{
			if (Entity != null) 
				throw new Exception("Component is already attached to an entity");
			_entity = entity;
		}
	}
}
