﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MonoGame.Extended;
using MonoGame.Extended.SceneGraphs;

namespace mxGame.Framework
{
	public class GameEntity : SceneEntity
	{
		new public Vector2 Position { get => _node.Position; set => _node.Position = value; }
		new public Vector2 WorldPosition { get => _node.WorldPosition; }
		new public float Rotation { get => _node.Rotation; set => _node.Rotation = value; }
		new public float WorldRotation { get => _node.Rotation;  }
		new public Vector2 Scale { get => _node.Scale; set => _node.Scale = value; }
		new public Vector2 WorldScale { get => _node.WorldScale; }

		private static uint _nextUID = 0;
		private static uint GetNextUID()
		{
			return ++_nextUID;
		}

		/// <summary>
		/// the unique Identifier of the entity
		/// </summary>
		public uint Uid => _uid;
		private uint _uid;

		/// <summary>
		/// the world node that this entity is attached to
		/// </summary>
		public WorldNode Node {
			get => _node;
			set {

				if (_node == value)
					return;

				WorldNode onode = _node;
				_node = value;

				if (value != null)
					value.Entity = this;
				if(onode != null)
					onode.Entity = null;
			}
		}
		private WorldNode _node;

		/// <summary>
		/// A user firendly name for the entity that can be changed
		/// </summary>
		public string Name { get; set; } = "GameEntity";

		/// <summary>
		/// creates an unregistered game entity, must be added to a world before it's used
		/// </summary>
		public GameEntity(string name = "GameEntity")
		{
			_uid = GetNextUID();
			Name = name;
		}

		/// <summary>
		/// creates a game entity and adds it to the specified world
		/// </summary>
		/// <param name="world">the world to add the game entity to</param>
		public GameEntity(GameWorld world, string name = "GameEntity") : this(name)
		{
			_node = new WorldNode(world);
			Name = name;
			
			_node.Entities.Add(this);
			world.RootNode.Children.Add(_node);
			world.AddEntity(this);
		}

		/// <summary>
		/// get the bounding rectangle of the object
		/// </summary>
		public override RectangleF BoundingRectangle
		{
			get => _node.World.GetEntityBoundingRectangle(Uid);
		}

		/// <summary>
		/// removes the entity from the game world
		/// </summary>
		public void RemoveEntity()
		{
			_node.World.RemoveEntity(Uid);
		}

		/// <summary>
		/// add a component of the specified type to the entity and return the instance of the 
		/// component that was created
		/// </summary>
		/// <typeparam name="T">The type of component to add</typeparam>
		public T AddComponent<T>() where T : Component, new()
		{
			return _node.World.AddEntityComponent<T>(Uid);
		}
	}
}
