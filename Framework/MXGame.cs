﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoShapes;

namespace mxGame.Framework
{
	public class MXGame : Game
	{
		// singleton pattern
		private static MXGame _instance;

		// singleton property getters
		public static MXGame Instance => _instance;
		public static OrthographicCamera MainCamera => _instance._mainCamera;
		public static GameFont DefaultFont => _instance._defaultFont;
		public static Texture2D Pixel => _instance._pixel;

		public Color backgroundColor = default;

		private GraphicsDeviceManager _graphics;
		private SpriteBatch _spriteBatch;
		private OrthographicCamera _mainCamera;
		private Debug.MiniStats _miniStats;
		private Texture2D _pixel;
		private GameFont _defaultFont;

		public GameWorld.Default World => _world;
		private GameWorld.Default _world;

		public MXGame() : base()
		{
			// enforce singleton pattern
			if(_instance != null) throw new System.Exception("MXGame singleton already instantiated");
			_instance = this;

			IsFixedTimeStep = false;
			MaxElapsedTime = System.TimeSpan.FromSeconds(0.1);
			_graphics = new GraphicsDeviceManager(this);
			backgroundColor = new Color(32, 32, 32);
			Content.RootDirectory = "Content";
			IsMouseVisible = true;
		}

		#region Game Implementation
		// override base game methods to create our own game functionality

		protected override void LoadContent()
		{
			_spriteBatch = new SpriteBatch(GraphicsDevice);
			_mainCamera = new OrthographicCamera(GraphicsDevice);
			_mainCamera.CenterAt(Vector2.Zero);

			_pixel = new Texture2D(GraphicsDevice, 1, 1);
			_pixel.SetData<Color>(new Color[1] { Color.White });
			Renderer.PrimitiveRenderer = new ShapeRenderer(GraphicsDevice);

			Texture2D fontTexture = Content.Load<Texture2D>("./Resources/defaultSpriteFont");
			_defaultFont = new GameFont(fontTexture);

			FileStream fileStream = new FileStream("./Content/Resources/defaultSpriteFont.json", FileMode.Open, FileAccess.Read);
			StreamReader fileReader = new StreamReader(fileStream);
			string json = fileReader.ReadToEnd();
			_defaultFont.SetConfigFromJSON(json);

			_miniStats = new Debug.MiniStats();
		}

		protected override void Initialize()
		{
			base.Initialize();

			_world = new GameWorld.Default();

			GameEntity entity = _world.CreateEntity("RandomObject");

			PolygonRenderer polyRdrr = entity.AddComponent<PolygonRenderer>();
			polyRdrr.Vertices = new Vector2[3] {
				new Vector2(100, 100),
				new Vector2(-100, 100),
				new Vector2(0, -100)
			};

			GameEntity entity2 = entity.Node.CreateChildEntity();
			entity2.Node.Position = new Vector2(100, 0);
			entity2.Node.Scale = new Vector2(0.25f, 0.25f);

			PolygonRenderer polyRdrr2 = entity2.AddComponent<PolygonRenderer>();
			polyRdrr2.Vertices = new Vector2[3] {
				new Vector2(-100, -100),
				new Vector2(100, -100),
				new Vector2(0, 100)
			};
		}

		protected override void Update(GameTime gameTime)
		{
			// exit game if escape or controller back is pressed
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			WorldNode node = (WorldNode)World.RootNode.Children[0];
			node.Rotation -= ((float)gameTime.ElapsedGameTime.TotalSeconds * 1);
			node.Position = new Vector2(
				MathF.Cos((float)gameTime.TotalGameTime.TotalSeconds * 2) * 100,
				MathF.Sin((float)gameTime.TotalGameTime.TotalSeconds) * 100
				);

			WorldNode node2 = (WorldNode)World.RootNode.Children[0].Children[0];

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			// clear the screen
			GraphicsDevice.Clear(backgroundColor);

			// get transformation matrix from camera
			Matrix viewMatrix = _mainCamera.GetViewMatrix();

			// GamePlay: --------------------------------------------------------------------------
			// begin gameplay rendering
			_spriteBatch.Begin(
				SpriteSortMode.FrontToBack,
				BlendState.NonPremultiplied,
				SamplerState.PointClamp,
				DepthStencilState.Default,
				RasterizerState.CullCounterClockwise,
				null,
				viewMatrix
			);

			_world.DrawArea = new RectangleF(new Vector2(-1000000), new Vector2(2000000));
			_world.RenderArea = new RectangleF(new Vector2(-1000000), new Vector2(2000000));
			_world.UpdateArea = new RectangleF(new Vector2(-1000000), new Vector2(2000000));
			_world.SetComponentSystemData(gameTime, _spriteBatch, GraphicsDevice);
			_world.HandleComponentSystems();

			// end gameplay rendering
			_spriteBatch.End();

			// UI: --------------------------------------------------------------------------------
			// begin UI rendering
			_spriteBatch.Begin(SpriteSortMode.BackToFront);

			// show debug stats
			_miniStats.Draw(gameTime, _spriteBatch, _defaultFont);

			// finish UI rendering
			_spriteBatch.End();

			base.Draw(gameTime);
		}

		#endregion
	}
}
