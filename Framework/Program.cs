﻿using System;

namespace mxGame.Framework
{
	public static class Program
	{
		[STAThread]
		static void Main()
		{
			using (MXGame game = new MXGame())
				game.Run();
		}
	}
}
