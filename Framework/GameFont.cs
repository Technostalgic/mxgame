﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json.Linq;

namespace mxGame.Framework
{
	public class GameFont
	{
		/// <summary>
		/// The texture that the gamefont uses to render the glyphs from
		/// </summary>
		public Texture2D Texture => _texture;
		private Texture2D _texture;

		private Rectangle[] _glyphRects;
		private int _defaultCharWidth;
		private int _defaultCharHeight;

		/// <summary>
		/// Create a new instance of a game font
		/// </summary>
		/// <param name="texture">the texture to be used for the spritefont</param>
		public GameFont(Texture2D texture)
		{
			_texture = texture;
		}

		/// <summary>
		/// Generate glyph rects from a uniform width and height, assuming the glyph sprites are 
		/// evenly spaced and sized on the spritesheet texture
		/// </summary>
		/// <param name="glyphs">the array of glyph characters that exist in the spritesheet</param>
		/// <param name="width">the width of each glyph sprite</param>
		/// <param name="height">the height of each glyph sprite</param>
		public void SetGlyphRects(char[] glyphs, int width, int height)
		{
			_defaultCharWidth = width;
			_defaultCharHeight = height;
			SetGlyphRects(glyphs, Texture.GetSpriteRects(width, height));
		}

		/// <summary>
		/// Set the glyph rects of the specified glyphs, 'glyphs' and 'rects' must be same 
		/// length array
		/// </summary>
		/// <param name="glyphs">the array of glyph characters that correspond to the rects 
		/// specified in the next parameter</param>
		/// <param name="rects"></param>
		public void SetGlyphRects(char[] glyphs, Rectangle[] rects)
		{
			if(glyphs.Length > rects.Length)
			{
				throw new Exception("glyphs:rects array length mismatch");
			}
			
			_glyphRects = new Rectangle[256];
			for(int i = glyphs.Length - 1; i >= 0; i--)
			{
				_defaultCharWidth = Math.Max(_defaultCharWidth, rects[i].Width);
				_defaultCharHeight = Math.Max(_defaultCharHeight, rects[i].Height);
				_glyphRects[glyphs[i]] = rects[i];
			}
		}

		/// <summary>
		/// Sets the gamefont configuration from properties specified in a json string
		/// </summary>
		/// <param name="json"></param>
		public void SetConfigFromJSON(string json)
		{
			// parse the json string into an enumerable object
			JObject parsed = JObject.Parse(json);
			_glyphRects = new Rectangle[256];

			// set the default width and height values
			_defaultCharWidth = parsed["maxSpriteWidth"].Value<int>();
			_defaultCharHeight = parsed["maxSpriteHeight"].Value<int>();

			// iterate through each glyph bounds definition
			JToken charsToken = parsed["characters"];
			foreach(JProperty glyph in charsToken)
			{
				// get the glyph character
				char key = glyph.Name[0];
				IEnumerable<JToken> values = glyph.Values();

				// get the glyph bounds rect from the json array value
				Rectangle bounds = new Rectangle();
				int i = 0;
				foreach(JToken value in values)
				{
					JValue val = value as JValue;
					if (val == null) continue;
					switch (i)
					{
						case 0: bounds.X = (int)Convert.ChangeType(val.Value, typeof(int)); break;
						case 1: bounds.Y = (int)Convert.ChangeType(val.Value, typeof(int)); break;
						case 2: bounds.Width = (int)Convert.ChangeType(val.Value, typeof(int)); break;
						case 3: bounds.Height = (int)Convert.ChangeType(val.Value, typeof(int)); break;
					}
					i++;
				}

				// set the glyph bounds
				_glyphRects[key] = bounds;
			}
		}

		/// <summary>
		/// Generates a texture with the specified string printed on it
		/// </summary>
		/// <param name="text"></param>
		/// <param name="spacing"></param>
		/// <param name="gd"></param>
		/// <param name="batch"></param>
		public Texture2D GenerateTexture(string text, int spacing, GraphicsDevice gd, SpriteBatch batch)
		{
			//RenderTargetBinding[] orts = gd.GetRenderTargets();

			// calculate the width and height of texture
			int width = 0;
			int height = 0;
			for(int i = text.Length - 1; i >= 0; i--)
			{
				Rectangle rect = _glyphRects[text[i]];
				width += rect.Width;
				height = Math.Max(height, rect.Height);
			}
			width += spacing * text.Length - 1;

			// create a texture to render the text onto
			RenderTarget2D rt = new RenderTarget2D(gd, width, height);
			gd.SetRenderTarget(rt);
			batch.Begin();

			// render each character onto the render target texture
			int xOff = 0;
			for (int i = text.Length - 1; i >= 0; i--)
			{
				Rectangle rect = _glyphRects[text[i]];
				batch.Draw(_texture, new Vector2(xOff, 0), Color.White);
				xOff += rect.Width + spacing;
			}

			// finalize rendering and remove rendertarget from graphics device
			batch.End();
			gd.SetRenderTarget(null);
			//gd.SetRenderTargets(orts);

			// return the texture that was created
			return rt;
		}

		/// <summary>
		/// Draws a string at the specified position with the specified settings
		/// </summary>
		/// <param name="position">the location of where the text should be drawn</param>
		/// <param name="text">the text to render</param>
		/// <param name="spacing">the spacing between each character</param>
		/// <param name="fixedWidth">whether or not each character in the string should be spaced 
		///		evenly so text columns line up</param>
		/// <param name="color">the color to apply to the text</param>
		/// <param name="batch">the spritebatch to use for rendering</param>
		public void DrawString(Vector2 position, string text, int spacing, bool fixedWidth, Color color, SpriteBatch batch)
		{
			// initialize a variable to keep track of the character offset from the origin position
			int xOff = 0;

			// iterate through each character in the string
			for (int i = 0; i < text.Length; i++)
			{
				// get the sprite rect for the current character
				Rectangle rect = _glyphRects[text[i]];

				// if text is rendering with fixed width, use default width/height
				if (fixedWidth)
				{
					rect.Width = _defaultCharWidth;
					rect.Height = _defaultCharHeight;
				}

				// render the glyph sprite at the specified position with applied offset
				batch.Draw(_texture, position + new Vector2(xOff, 0), rect, color);

				// increment the offset by the sprite width and specified character spacing
				xOff += rect.Width + spacing;
			}
		}
	}
}
